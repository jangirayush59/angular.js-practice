function func() {
    var a = 0;

    $(document).ready(function () {

        console.log($(window).width())

        if ($(window).width() < 550) {
            $('.form-group').removeClass('row');
        }

        $(window).resize(function () {
            if ($(window).width() < 550 && a === 0) {
                $('.form-group').removeClass('row');
            } else {
                $('.form-group').addClass('row');
            }
        })

        $(document).scroll(function () {
            if ($(document).scrollTop() > 50) {
                $('#scroll-prompt').removeClass('animated');
            } else {
                $('#scroll-prompt').addClass('animated');
            }
        });

        $('#submit-button').click(function () {
            a = 1 - a;
            if (a) logIn(); else logOut();
        });
    });
}

function logIn() {
    ld = $('#inputLDAP');
    ld.addClass('form-control-plaintext');
    ld.prop('disabled', true);
    $('#pass-div').prop('hidden', true);
    $('#name-div').prop('hidden', false);
    $('#resume').prop('hidden', false);
    $('.form-group').addClass('row');
    $('#submit-button').html('Log Out');
    $('#prompt').html('Hello Ayush! Kindly upload your resume.');
}

function logOut() {
    ld = $('#inputLDAP');
    ld.removeClass('form-control-plaintext');
    ld.prop('disabled', false);
    $('#pass-div').prop('hidden', false);
    $('#name-div').prop('hidden', true);
    $('#resume').prop('hidden', true);
    $('#resume').val(null);
    if ($(window).width() < 550) {
        $('.form-group').removeClass('row');
    }
    $('#submit-button').html('Log In');
    $('#prompt').html('Log in with your LDAP credentials.');
}