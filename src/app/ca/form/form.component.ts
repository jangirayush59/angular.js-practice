import { Component, OnInit } from '@angular/core';
import * as Rellax from 'rellax';
declare function func(): any;

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  data: Date = new Date();

  constructor() { }

  ngOnInit() {
    func();
    var rellaxHeader = new Rellax('.rellax');
  }

}
